# SPECIFICATION
  NAME <- "StepMLR.R"
  # FUNCTIONS: 
  #   1. STEP multiple linear regression FOR acute_toxicity prediction
  #   2. ...
  # AUTHOR:xyw
  # NOTE
  #   1. This R script is tested in R.3.0.1 on WIN32 platform.
  #   2. ...
# LOG
  #   VERSION: 2013.12.25
  #   VERSION: 2013.11.27
  #   VERSION: 2013.11.20
  
# CODES  
  print(paste("Loading ",NAME,"..."))
  rm(NAME)
  ## packages("pls") for multiple linear regression
  # library('pls') # NO NEED TO LOAD IN R.3.0.2
  ## 残差分析说明
  ### Residual_Error_Analysis_Meaning
REAM<- function() {
  Residual_Error_Analysis_Meaning <- 
    strwrap("
            得到的四个图依次为：
            4.1普通残差与拟合值的残差图
            4.2正态QQ的残差图（若残差是来自正态总体分布的样本，则QQ图中的点应该在一条直线上）
            4.3标准化残差开方与拟合值的残差图（对于近似服从正态分布的标准化残差，应该有95%的样本点落在[-2,2]的区间内。这也是判断异常点的直观方法）
            4.4cook统计量的残差图（cook统计量值越大的点越可能是异常值，但具体阀值是多少较难判别）
            从图中可见，xx样本存在异常，需要剔除。
            ")
  print(Residual_Error_Analysis_Meaning)
}
REAM()
  ## step lm function
  ### use step lm mothod... multiple linear regression
  ### yeli:it better to GA-PLS agriolthm to select descripters.
  ### Args:
  ###     data_in:    data frame input
  ###     class_:     dependent variable. One dimmer name of data_in, which will be predicted
  ###     descriptors: independent variables.
  ###     verbose:    If TRUE, prints sample covariance; if not, not. Default is TRUE.
  ### 
  ### Returns:
  ###     None.
StepMLR <- function(data_in
                    , class_
                    , descriptors
                    , ResidualE_analysis = FALSE
                    , verbose = TRUE
                    ) {
  data_in$class_  # no success to display data_in$ld50
  f <- paste(class_ , "~", descriptors)
  lm.test <- lm(f, data=data_in)
  if (ResidualE_analysis) {
    #print(paste("lm.test",lm.test))
    #### 用残差分析 show and 剔除异常点
    REAM()
    #### plot(lm.test, which = 1:4)
    plot(lm.test, which = 1:4,id.n=10,labels.id=NULL) 
    #### plot is short for plot.lm
  }
  
  step.acute <- step(lm.test, direction = "both")
  drop.acute <- drop1(step.acute)
  print(summary(step.acute)) #### $coefficients str()
  
  if (verbose) {
    pred_step.acute <- predict(step.acute,newdata=data_in)
    #### pred_step.acute
    x <- as.vector(as.matrix(data_in[class_]))
    y <- pred_step.acute
    axisRange <- extendrange(c(x, y))
    plot( x ,y, ylim = axisRange, xlim = axisRange,
          , xlab="Observed value "
          , ylab="Predicted value ",)
    # text(x, y, labels=row(as.matrix(x))) # identify points 
    abline(0, 1, col = "red", lty = 2)
    predicted <- y; observed <- x;
    rr <- round(cor(predicted, observed)^2, 4)
    text(trunc(max(x,y)*0.4), trunc(max(x,y)*0.94), labels=paste("Multiple R-squared: ",rr) )
    # title("Predicted VS Observed values")
  }
  return(step.acute)
}
  
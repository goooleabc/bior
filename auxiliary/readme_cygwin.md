 author: xyw  
 date: 2013.09.23  
 update: 2018.09.30 
 For xyw's google program  
# Cygwin workshop in Windows
-----------------------
* install the latest version of Cygwin.  
> + download the latest version of Cygwin from https://cygwin.com/.
> + setup the file �setup-x86_64.exe�
> + select the default base packages  
> + select your favourite packages: mc, vim, lynx, R, perl  
> + select git core paceages


* Do not install the windows version of Git
> + "select the command mode to install, which can fit for Cygwin well."
  The Git-1.8.1.2-preview20130201.exe is not good at fitting for Cygwin_NI-5.1.  

That's all.
---------------------------
# bash commonds
1. Display Filesystem  
	`df -H`
1. Display Usage  
	`du -H`
1. Print Working Directory  
	`pwd`
    
# git cmds
 # .26  
git clone url/repo.git myrepo  
git remote add origin url/repo.git  
 
 # some modifications in origin on the remote
# fecth the origin from remote
git fetch origin  
# get the difference of . and origin
git diff origin  
git log -p master..origin  
git merge origin  




"# xyw's vim setup file
"# Log
"# 2013.11.20
" "optimized the fraction of encoding "
"# 2013.11.01
" "optimized"
" date: 2013.09.23

"==========================================
" General 基础设置
"==========================================
" history存储长度。
set history=2000
"检测文件类型
filetype on
"针对不同的文件类型采用不同的缩进格式
filetype indent on
" 取消备份。 视情况自己改
set nobackup
set noswapfile

" set cursorcolumn
set cursorline              " 突出显示当前行
"设置 退出vim后，内容显示在终端屏幕, 可以用于查看和复制
"好处：误删什么的，如果以前屏幕打开，可以找回
"set t_ti= t_te=

"==========================================
" file encode, 文件编码,格式
"==========================================
" 设置新文件的编码为 UTF-8
"set fileencodings=ucs-bom,utf-8,gb2312,big5,latin1
if has("multi_byte")
  if &termencoding == ""
    let &termencoding = &encoding
  endif
    set encoding=utf-8
    " 自动判断编码时，依次尝试以下编码：
    set fileencodings=ucs-bom,utf-8,cp936,gb18030,big5,euc-jp,euc-kr,latin1
    "set fileencodings=gbk,gb2312,utf-8,gb18030
    " set termencoding=gbk
endif
"删除多余空格
" Delete trailing white space on save, useful for Python and CoffeeScript ;)
func! DeleteTrailingWS()
  exe "normal mz"
  %s/\s\+$//ge
  exe "normal `z"
endfunc
autocmd BufWrite *.pl :call DeleteTrailingWS()

" Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

"==========================================
" Show 展示/排班等界面格式设置
"==========================================
"显示行号：
set number
" set nowrap                    " 取消换行。

"括号配对情况
set showmatch
" How many tenths of a second to blink when matching brackets
set mat=2

"设置文内智能搜索提示
" 高亮search命中的文本。
set hlsearch
" 搜索时忽略大小写
set ignorecase
" 随着键入即时搜索
set incsearch
" 有一个或以上大写字母时仍大小写敏感
set smartcase     " ignore case if search pattern is all lowercase, case-sensitive otherwise


set tabstop=4                " 设置Tab键的宽度        [等同的空格个数]
set shiftwidth=4  " number of spaces to use for autoindenting
" set softtabstop=4             " 按退格键时可以一次删掉 4 个空格
set smarttab      " insert tabs on the start of a line according to shiftwidth, not tabstop 按退格键时可以一次删掉 4 个空格

set expandtab                " 将Tab自动转化成空格    [需要输入真正的Tab键时，使用 Ctrl+V + Tab]


" show the cursor position always 
set ruler
""在状态栏显示正在输入的命令
set showcmd
" Show current mode
set showmode


"==========================================
" 快捷键
"==========================================
""为方便复制，用<F2>开启/关闭行号显示:
nnoremap <F2> :set nonumber! number?<CR>
nnoremap <F3> :set list! list?<CR>
nnoremap <F4> :set wrap! wrap?<CR>
nnoremap <F6> :exec exists('syntax_on') ? 'syn off' : 'syn on'<CR>
nnoremap <leader>q :q<CR>
" select all
"map <Leader>sa ggVG"
" Opens a new tab with the current buffer's path
" Super useful when editing files in the same directory
"map <leader>te :tabedit <c-r>=expand("%:p:h")<cr>/

"==========================================
" 语法配色显示等
"==========================================
" 代码折叠
set foldenable
" 折叠方法
" manual    手工折叠
" indent    使用缩进表示折叠
" expr      使用表达式定义折叠
" syntax    使用语法定义折叠
" diff      对没有更改的文本进行折叠
" marker    使用标记进行折叠, 默认标记是 {{{ 和 }}}
set foldmethod=indent
set foldlevel=99
"Smart indent
set smartindent
set autoindent    " always set autoindenting on

set nocompatible " 关闭 vi 兼容模式
syntax on " 自动语法高亮
colorscheme desert " 设定配色方案
" set number " 显示行号
"set cursorline " 突出显示当前行
" set ruler " 打开状态栏标尺
" set shiftwidth=4 " 设定 << 和 >> 命令移动时的宽度为 4
set softtabstop=2 " 使得按退格键时可以一次删掉 4 个空格
" set tabstop=4 " 设定 tab 长度为 4
" set nobackup " 覆盖文件时不备份
" set autochdir " 自动切换当前目录为当前文件所在的目录
filetype plugin indent on " 开启插件
" set backupcopy=yes " 设置备份时的行为为覆盖
"set ignorecase smartcase " 搜索时忽略大小写，但在有一个或以上大写字母时仍保持对大小写敏感
" set nowrapscan " 禁止在搜索到文件两端时重新搜索
" set incsearch " 输入搜索内容时就显示搜索结果
"set hlsearch " 搜索时高亮显示被找到的文本
" set noerrorbells " 关闭错误信息响铃
" set novisualbell " 关闭使用可视响铃代替呼叫
" set t_vb= " 置空错误铃声的终端代码
" set showmatch " 插入括号时，短暂地跳转到匹配的对应括号
" set matchtime=2 " 短暂跳转到匹配括号的时间
" set magic " 设置魔术
" set hidden " 允许在有未保存的修改时切换缓冲区，此时的修改由 vim 负责保存
" set guioptions-=T " 隐藏工具栏
" set guioptions-=m " 隐藏菜单栏
" set smartindent " 开启新行时使用智能自动缩进
"set backspace=indent,eol,start
" " 不设定在插入状态无法用退格键和 Delete 键删除回车符
" set cmdheight=1 " 设定命令行的行数为 1
set laststatus=2 " 显示状态栏 (默认值为 1, 无法显示状态栏)
set statusline=\ %<%F[%1*%M%*%n%R%H]%=\ %y\ %0(%{&fileformat}\ %{&fileencoding}\ %c:%l/%L%)\ 
" " 设置在状态行显示的信息
" set foldenable " 开始折叠
" set foldmethod=syntax " 设置语法折叠
" set foldcolumn=0 " 设置折叠区域的宽度
" setlocal foldlevel=1 " 设置折叠层数为
" set foldclose=all " 设置为自动关闭折叠 
nnoremap <space> @=((foldclosed(line('.')) < 0) ? 'zc' : 'zo')<CR>
" " 用空格键来开关折叠

# stat.terms.mlr.md
# LOG
    # 2013.11.20,xyw
    #  add warning message of predict.lm


1、整理原始数据
2、做一次线性分析，如果合理就ok
3、不合理的情况下做二次线性分析，根据X的元素数，产生完整的二次函数的完整因子项
4、通过STEP函数干掉显著性最差的因子项
5、不断通过SUMMARY函数干掉显著性最差的因子项，重建模型再做SUMMARY函数分析直到结果满意且不做过度删除

SUMMARY函数的结果判断依据
1、每个因子项本身有显著性判断，Pr(>|t|)越小越好
2、Residual standard error，残差的标准差，越小越好
3、Multiple R-squared, Adjusted R-squared ，相关系数，越接近1越好
4、p-value，关于F分布的P值，显著性标准，越小越好
## 
    Warning message:
    > prediction from a rank-deficient fit may be misleading in:
    > predict.lm(object, newdata)
From ?predict.lm
If the fit is rank-deficient, some of the columns of the design matrix will have been dropped.  Prediction from such a fit only
makes sense if 'newdata' is contained in the same subspace as the original data.  That cannot be checked accurately, so a warning is
issued.

# Elements of an OLS Regression Equation
http://webhelp.esri.com/arcgisdesktop/9.3/index.cfm?TopicName=Regression_analysis_basics
Dependent variable (y): this is the variable representing the process you are trying to predict or understand (e.g., residential burglary, foreclosure, rainfall). In the regression equation, it appears on the left side of the equal sign. While you can use regression to predict the dependent variable, you always start with a set of known y values and use these to build (or to calibrate) the regression model. The known y values are often referred to as observed values.  
Independent/Explanatory variables (X): these are the variables used to model or to predict the dependent variable values. In the regression equation, they appear on the right side of the equal sign and are often referred to as explanatory variables. We say that the dependent variable is a function of the explanatory variables. If you are interested in predicting annual purchases for a proposed store, you might include in your model explanatory variables representing the number of potential customers, distance to competition, store visibility, and local spending patterns, for example.  
Regression coefficients (β): coefficients are computed by the regression tool. They are values, one for each explanatory variable, that represent the strength and type of relationship the explanatory variable has to the dependent variable. Suppose you are modeling fire frequency as a function of solar radiation, vegetation, precipitation and aspect. You might expect a positive relationship between fire frequency and solar radiation (the more sun, the more frequent the fire incidents). When the relationship is positive, the sign for the associated coefficient is also positive. You might expect a negative relationship between fire frequency and precipitation (places with more rain have fewer fires). Coefficients for negative relationships have negative signs. When the relationship is a strong one, the coefficient is large. Weak relationships are associated with coefficients near zero.   
β 0 is the regression intercept. It represents the expected value for the dependent variable if all of the independent variables are zero.  
##P-Values:
 most regression methods perform a statistical test to compute a probability, called a p-value, for the coefficients associated with each independent variable. The null hypothesis for this statistical test states that a coefficient is not significantly different from zero (in other words, for all intents and purposes, the coefficient is zero and the associated explanatory variable is not helping your model). Small p-values reflect small probabilities, and suggest that the coefficient is, indeed, important to your model with a value that is significantly different from zero (the coefficient is NOT zero). You would say that a coefficient with a p value of 0.01, for example, is statistically significant at the 99% confidence level; the associated variable is an effective predictor. Variables with coefficients near zero do not help predict or model the dependent variable; they are almost always removed from the regression equation, unless there are strong theoretical reasons to keep them. 
##R 2 /R-Squared:
 Multiple R-Squared and Adjusted R-Squared are both statistics derived from the regression equation to quantify model performance. The value of R-squared ranges from 0 to 100 percent. If your model fits the observed dependent variable values perfectly, R-squared is 1.0 (and you, no doubt, have made an error… perhaps you've used a form of y to predict y). More likely, you will see R-squared values like 0.49, for example, which you can interpret by saying: this model explains 49% of the variation in the dependent variable. To understand what the R-squared value is getting at, create a bar graph showing both the estimated and observed Y values sorted by the estimated values. Notice how much overlap there is. This graphic provides a visual representation of how well the model's predicted values explain the variation in the observed dependent variable values. View an illustration. The Adjusted R-Squared value is always a bit lower than the Multiple R-Squared value because it reflects model complexity (the number of variables) as it relates to the data. 
##Residuals:
 these are the unexplained portion of the dependent variable, represented in the regression equation as the random error term, ε. View an illustration. Known values for the dependent variable are used to build and to calibrate the regression model. Using known values for the dependent variable (y) and known values for all of the explanatory variables (the Xs), the regression tool constructs an equation that will predict those known y values, as well as possible. The predicted values will rarely match the observed values exactly. The difference between the observed y values and the predicted y values are called the residuals. The magnitude of the residuals from a regression equation is one measure of model fit. Large residuals indicate poor model fit.   

Building a regression model is an iterative process that involves finding effective independent variables to explain the process you are trying to model/understand, then running the regression tool to determine which variables are effective predictors… then removing/adding variables until you find the best model possible.


F值是方差分析中的一个指标，一般方差分析是比较组间差异的。F值越大，P值越小，表示你的结果越可靠。比如，你的结果表明三组之间差别有统计学意义，P值如果等于0.01，表示有1-0.01=0.99即99%的把握认为你的结论是正确的。


F检验是用来检验总体回归关系的显著性（其结果与与预先给定的显著性水平比较说明总体的显著性的好坏。）与之还有T检验，不过T检验是检验各个回归系数的显著性。

http://blog.znsun.com/2008/04/653/t-test-and-f-test-and-p-or-sig-value
T检验、F检验和统计学意义（P值或sig值）

1.T检验和F检验的由来

一般而言，为了确定从样本(sample)统计结果推论至总体时所犯错的概率，我们会利用统计学家所开发的一些统计方法，进行统计检定。

通过把所得到的统计检定值，与统计学家建立了一些随机变量的概率分布(probability distribution)进行比较，我们可以知道在多少%的机会下会得到目前的结果。倘若经比较后发现，出现这结果的机率很少，亦即是说，是在机会很少、很罕有的情况下才出现；那我们便可以有信心的说，这不是巧合，是具有统计学上的意义的(用统计学的话讲，就是能够拒绝虚无假设null hypothesis,Ho)。相反，若比较后发现，出现的机率很高，并不罕见；那我们便不能很有信心的直指这不是巧合，也许是巧合，也许不是，但我们没能确定。

F值和t值就是这些统计检定值，与它们相对应的概率分布，就是F分布和t分布。统计显著性（sig）就是出现目前样本这结果的机率。

2. 统计学意义（P值或sig值）

结果的统计学意义是结果真实程度（能够代表总体）的一种估计方法。专业上，p值为结果可信程度的一个递减指标，p值越大，我们越不能认为样本中变量的关联是总体中各变量关联的可靠指标。p值是将观察结果认为有效即具有总体代表性的犯错概率。如p=0.05提示样本中变量关联有5%的可能是由于偶然性造成的。即假设总体中任意变量间均无关联，我们重复类似实验，会发现约20个实验中有一个实验，我们所研究的变量关联将等于或强于我们的实验结果。（这并不是说如果变量间存在关联，我们可得到5%或95%次数的相同结果，当总体中的变量存在关联，重复研究和发现关联的可能性与设计的统计学效力有关。）在许多研究领域，0.05的p值通常被认为是可接受错误的边界水平。

3. T检验和F检验

至於具体要检定的内容，须看你是在做哪一个统计程序。

举一个例子，比如，你要检验两独立样本均数差异是否能推论至总体，而行的t检验。
两样本(如某班男生和女生)某变量(如身高)的均数并不相同，但这差别是否能推论至总体，代表总体的情况也是存在著差异呢？
会不会总体中男女生根本没有差别，只不过是你那麼巧抽到这2样本的数值不同？
为此，我们进行t检定，算出一个t检定值。
与统计学家建立的以「总体中没差别」作基础的随机变量t分布进行比较，看看在多少%的机会(亦即显著性sig值)下会得到目前的结果。
若显著性sig值很少，比如<0.05(少於5%机率)，亦即是说，「如果」总体「真的」没有差别，那麼就只有在机会很少(5%)、很罕有的情况下，才会出现目前这样本的情况。虽然还是有5%机会出错(1-0.05=5%)，但我们还是可以「比较有信心」的说：目前样本中这情况(男女生出现差异的情况)不是巧合，是具统计学意义的，「总体中男女生不存差异」的虚无假设应予拒绝，简言之，总体应该存在著差异。

每一种统计方法的检定的内容都不相同，同样是t-检定，可能是上述的检定总体中是否存在差异，也同能是检定总体中的单一值是否等於0或者等於某一个数值。

至於F-检定，方差分析(或译变异数分析，Analysis of Variance)，它的原理大致也是上面说的，但它是透过检视变量的方差而进行的。它主要用于：均数差别的显著性检验、分离各有关因素并估计其对总变异的作用、分析因素间的交互作用、方差齐性(Equality of Variances)检验等情况。

4. T检验和F检验的关系

t检验过程，是对两样本均数(mean)差别的显著性进行检验。惟t检验须知道两个总体的方差(Variances)是否相等；t检验值的计算会因方差是否相等而有所不同。也就是说，t检验须视乎方差齐性(Equality of Variances)结果。所以，SPSS在进行t-test for Equality of Means的同时，也要做Levene's Test for Equality of Variances 。

4.1
在Levene's Test for Equality of Variances一栏中 F值为2.36, Sig.为.128，表示方差齐性检验「没有显著差异」，即两方差齐(Equal Variances)，故下面t检验的结果表中要看第一排的数据，亦即方差齐的情况下的t检验的结果。

4.2.
在t-test for Equality of Means中，第一排(Variances=Equal)的情况：t=8.892, df=84, 2-Tail Sig=.000, Mean Difference=22.99
既然Sig=.000，亦即，两样本均数差别有显著性意义！

4.3
到底看哪个Levene's Test for Equality of Variances一栏中sig,还是看t-test for Equality of Means中那个Sig. (2-tailed)啊?
答案是：两个都要看。
先看Levene's Test for Equality of Variances，如果方差齐性检验「没有显著差异」，即两方差齐(Equal Variances)，故接著的t检验的结果表中要看第一排的数据，亦即方差齐的情况下的t检验的结果。
反之，如果方差齐性检验「有显著差异」，即两方差不齐(Unequal Variances)，故接著的t检验的结果表中要看第二排的数据，亦即方差不齐的情况下的t检验的结果。

4.4
你做的是T检验，为什么会有F值呢?
就是因为要评估两个总体的方差(Variances)是否相等，要做Levene's Test for Equality of Variances，要检验方差，故所以就有F值。

另一种解释：

t检验有单样本t检验，配对t检验和两样本t检验。

单样本t检验：是用样本均数代表的未知总体均数和已知总体均数进行比较，来观察此组样本与总体的差异性。

配对t检验：是采用配对设计方法观察以下几种情形，1，两个同质受试对象分别接受两种不同的处理；2,同一受试对象接受两种不同的处理；3，同一受试对象处理前后。

F检验又叫方差齐性检验。在两样本t检验中要用到F检验。

从两研究总体中随机抽取样本，要对这两个样本进行比较的时候，首先要判断两总体方差是否相同，即方差齐性。若两总体方差相等，则直接用t检验，若不等，可采用t'检验或变量变换或秩和检验等方法。

其中要判断两总体方差是否相等，就可以用F检验。

若是单组设计，必须给出一个标准值或总体均值，同时，提供一组定量的观测结果，应用t检验的前提条件就是该组资料必须服从正态分布；若是配对设计，每对数据的差值必须服从正态分布；若是成组设计，个体之间相互独立，两组资料均取自正态分布的总体，并满足方差齐性。之所以需要这些前提条件，是因为必须在这样的前提下所计算出的t统计量才服从t分布，而t检验正是以t分布作为其理论依据的检验方法。

简单来说就是实用T检验是有条件的，其中之一就是要符合方差齐次性，这点需要F检验来验证。

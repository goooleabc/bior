# SPECIFICATION
  NAME <- "acute_tox_R_O_pre.2.R"
  # FUNCTIONS: 
  #   1. build final category models
  #   2. todo: 
  # AUTHOR:xyw
  # NOTE
  #   1. This R script is tested in R.3.0.2 on WIN32 platform.
  #   2. ...
# LOG
  VERSION <- "2014.03.10"
  # VERSION <- "2014.03.10"
  #     mininal edition
  
  # VERSION <- "2014.02.26"
  #     created.
  #
  
  print(paste("Loading ",NAME,". Version ", VERSION))
  rm(NAME, VERSION)
  
  # @MOLFORMULA ,  should be a character vector
  # required loading caret packages(library("caret"))
  training   #
  testing    #
  des_train  #
  
  #### this followed lines are in acute_tox_R_O_pre.2.R. ####
  des_train  <- training[, -1]
  ld50_train <- training[,1]
  ## group data with method of Hierarchical Clustering 
  LOAD_FUNC("h.clust",ls())
  # dist(des_train)
  # hclust(dist(des_train),method="ward")
  groups <- h.clust(des_train,"ward",2)
  names(groups)
  ###########################################################
  ## Notice this type change
  groups <- as.matrix(groups)
  head(groups[, 1])
  summary(groups)
  table(groups)
  
  
  ## assemble data.frame for modeling
  des_train_m <- as.matrix(des_train)
  str(des_train_m[1:5, 1:5])
  training_total <- data.frame("class"=groups[,1]
                           ,"acuteCla"=ld50_train
                           ,"acuteDes4"=I(des_train_m))
  str(training_total[1:5, 1:5])
  data_1_flag <- training_total$class == 1
  data_1_flag
  training_1      <- data.frame("ld50"=training_total$acuteCla[data_1_flag]
                            ,"des4"=I(training_total$acuteDes4[data_1_flag,]))
  ### make sure data.frame well
  training_1[1:2, 1:5]
  dim(training_total[c(1,3), ])
  
  data_2_flag <- training_total$class == 2
  training_2      <- data.frame("ld50"=training_total$acuteCla[data_2_flag]
                            ,"des4"=I(training_total$acuteDes4[data_2_flag,]))
  rm(training_total, data_1_flag, data_2_flag)
  
  training_sum <-  data.frame("ld50" = ld50_train, "des4" = I(des_train_m))
  str(training_sum)
  
  
  ## svm models
#   fitControl <- trainControl(## 10-fold CV
#     method = "repeatedcv",
#     number = 10,
#     ## repeated ten times
#     repeats = 10
#   )

  #install.packages(c("kernlab","k1aR"))
  svmFit_1 <- train(ld50 ~ des4, data = training_1
                  , method = "svmRadial"
                  , trControl = fitControl
                  , preProc = c("center", "scale")
                  , tuneLength = 8
                  #, metric = "ROC"
  )
  svmFit_1
  svmFit_2 <- train(ld50 ~ des4, data = training_2
                    , method = "svmRadial"
                    , trControl = fitControl
                    , preProc = c("center", "scale")
                    , tuneLength = 8
                    #, metric = "ROC"
  )
  svmFit_2
  #### notes ####
  #### (1) To see the Model methods' names
  ####    R command line> names(getModelInfo())
  #### (2) Use function load.lib for loading a package
  ####    "load.lib" is in "R_scripts/functions/LOAD_FUNC.R".
  ####    load.lib can install a package which is not in the system.
  load.lib("rknn")
  rknnFit_1 <- train(ld50 ~ des4, data = training_1
                     , method = "rknn"
                     , trControl = fitControl
  )
  rknnFit_2 <- train(ld50 ~ des4, data = training_2
                     , method = "rknn"
                     , trControl = fitControl
  )
  rfFit_2 <- train(ld50 ~ des4, data = training_2
                   , method = "rf"
                   , trControl = fitControl
  )
  rfFit_2
  
  load.lib("kknn")
  kknnFit_1 <- train(ld50 ~ des4, data = training_1
                     , method = "kknn"
                     , trControl = fitControl
  )
  kknnFit_2 <- train(ld50 ~ des4, data = training_2
                     , method = "kknn"
                     , trControl = fitControl
  )
  
######## note #################
#   With two categories of the training data,
#     the models built were no better than "StemMLR" model.
#   Then I will try to split the training data to more categories( 4, or 5).
#   The following scripts are in acute_tox_R_O_pre.3.R.
  
  
  ## modeling by step multiple linear regression 
  LOAD_FUNC("StepMLR",1)
  data_lm   <- StepMLR(data_total,"ld50","des4",,)
  str(data_lm)
  # calc the 赤池信息统计量
  AIC(data_lm) 
  # normality test for residuals
  shapiro.test(data_1_lm$residuals) 
  
  #### Prediction  from linear model 
  Predict_Show <- function (model,testData,val_obs_name) {
    pred_step.acute <- predict.lm(model,newdata=testData)
    x <- as.vector(as.matrix(testData[val_obs_name]))
    y <- pred_step.acute
    print(x)
    print(y)
    print(summary(y))
    plot( x , y
          , xlab="Observed acute value (log(LD50 mg/kg))"
          , ylab="Predicted acute value (log(LD50 mg/kg))",)
    text(x, y, labels=row(as.matrix(x))) # identify points 
    lines(-1:5,-1:5)
    title("Predicted VS Observed LD50")
  }
  
  Predict_Show(data_lm,data_1_test,"ld50")

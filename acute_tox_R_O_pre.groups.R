# SPECIFICATION
  # NAME: acute_tox_R_O_pre.R
  #     急毒，rat-大鼠，oral-口服
  # FUNCTIONS:
  #    1. Predict the acute toxicity(Rat Oral LD50) of compounds by QSAR methods,
  #      ie.,ward's cluster and MLSR.
  # AUTHOR:xyw
  # NOTE
  #   1. This R script is tested in R.3.0.1 on WIN32 platform.
  #   2. ...
# LOG

  # VERSION: 2013.12.20
  #   1. creat function Select_Organic
  #    to delete inorganic and organometallic compounds.
  #   2. optimized Normalize_Data function
  #    testTransformed <- predict(preProcValues, test)
  #     here, preProcValues <- preProcess(training, method=c("center", "scale"))
  #
  # VERSION: 2013.12.18
  #   1. After deleting inorganic and organometallic compounds, 
  #     the variables of obs. were also high relative. findCorrelation, left 2v
  #   2. Then I thoult i should unique the rows of descriptors data.
  #     Note that, 
  #       there were some compounds which had not structure in the TOXFIND DB.
  #       These compounds had same descriptors by MOLD2.
  #
  # VERSION: 2013.12.16
  #   Try to get rid off some n/a obs.
  #
  # VERSION: 2013.12.12
  #   created.
  #


# CODES
  # set working directory

if (Sys.info()["sysname"] == "Windows") {
  setwd("F:/work/tox_predict/acutetoxicity_LD50_QSAR_Analysis/")
  # source LOAD_FUNC script for loading fucntions
  Bio_R_func_dir <- "C:/Documents and Settings/xwang/My Documents/BioR/functions/"
  source("C:/Documents and Settings/xwang/My Documents/BioR/functions/LOAD_FUNC.R")
} else {
  setwd("/home/xwang/acute_pre/")
  Bio_R_func_dir <- "/home/xwang/acute_pre/R_scripts/functions/"
  source("./R_scripts/functions/LOAD_FUNC.R")
}
  
## Read data
  # Data_select <- read.table(file = file.choose(), sep="\t", header=TRUE)
 #### Read descriptors
  file_in   <- "./data_collected_2013.11.21/RTECS__Rat_O_LD50_16076.sdf.mold2.out"
  Data <- read.table(file_in, sep="\t", header=TRUE)
  Data[1:5, 1:5] # list to see
  # # get rid off the first column which is the index number
  Desc <- Data[, -1]
  # get the unique desc rows, 13225 obs/rows
  Desc_u <- unique(Desc)
  #rm(Data)
 #### Read LD50 etc.
  file_in   <- "./data_collected_2013.11.21/RTECS__Rat_O_LD50_16076.sdf.csv"
  ACUTE_TOX <- read.table(file_in, sep=",", header=TRUE)
  ACUTE_TOX$Number = c(1: length(rownames(ACUTE_TOX))) # 16076 
  ACUTE_TOX[1:5,]
  ## Read mol formula
  file_in   <- "./data_collected_2013.11.21/rtecs_r_o_ld50_16067_formula.mdb.csv"
  formula <- read.table(file_in, sep=",", header=TRUE)
  formula[1:5,]
  data_tox  <- ACUTE_TOX
  #data_tox$mdlnum_f   <- formula$MDLNUM
  data_tox$MOLFORMULA <- formula$MOLFORMULA
  head(data_tox);
  str(data_tox)

  # syn data_tox and Desc_u
  data_tox <- data_tox[rownames(Desc_u),]
  rm(file_in, ACUTE_TOX, formula)
  #summary(data_tox$mdlnum_f == data_tox$MDLNUM)


  # get rid off rows of the dose values with ">"
  flag_text   <- grep(">",data_tox$DOSE_TEXT)
  data_tox_t   <- data_tox[-flag_text,]
  str(data_tox_t)
  # get rid off rows of the mol weight <= 0
  # according to Mr.zhu's suggestion, the mol weight >150 & <= 1000
  flag_weight <- data_tox_t$MOLWEIGHT > 150 & data_tox_t$MOLWEIGHT <= 1000
  table(flag_weight)
  data_tox_w <- data_tox_t[c(1: length(rownames(data_tox_t)))[flag_weight],]
  str(data_tox_w)
  # get rid off rows of the unit != "mg/kg"
  flag_unit   <- data_tox_w$DOSE_UNITS == "mg/kg"
  table(flag_unit)
  data_tox_u <- data_tox_w[c(1: length(rownames(data_tox_w)))[flag_unit],]
  str(data_tox_u)
  # 9083 obs.
  rm(flag_text, data_tox_t, flag_weight, data_tox_w, flag_unit )
  str(rownames((data_tox_u)))
  # removed inorganic and organometallic compounds, salts.
  LOAD_FUNC("Select_Organic", 1)
  flag_o <- Select_Organic(as.vector(data_tox_u$MOLFORMULA))
  tail(flag_o)
  data_tox_o <- data_tox_u[flag_o, ]
  tail(data_tox_o$MOLFORMULA)
  str(data_tox_o)
  rm(flag_o, data_tox_u)

  ## convert ld50 to log10(1/C), where C is the molar lethal dose
  ## C(mol/kg), the unit of C must be mol/kg.
  if (FALSE) {  # NOT be "false" here
    ld50 <- -log(data_tox_o$DOSE_VALUE/1000/data_tox_o$MOLWEIGHT,base=10)
    tail(ld50)
  }
  ## according to Mr.zhu's suggestion, use tox classification method
  tox_classification <- function(ld50_v) {
    #ld50_v <- data_tox_o$DOSE_VALUE
    # ld50_v(oral,ld50) value numeric, mg/kg
    f_violent <- ld50_v < 5
    f_high    <- ld50_v >= 5 & ld50_v < 50
    f_medium  <- ld50_v >=50 & ld50_v <=500
    f_low     <- ld50_v > 500
    ld50      <- data.frame("ld50"=ld50_v, "cla" = "0")
    # str(ld50)
    ld50$cla  <- as.character(ld50$cla)
    
    ld50$cla[f_violent] <- "voilent"  # 249
    ld50$cla[f_high   ] <- "high"     # 688
    ld50$cla[f_medium ] <- "medium"   # 2087
    ld50$cla[f_low    ] <- "low"      # 4385
    # table(ld50$cla)
    # rm(f_violent, f_high, f_medium, f_low, len, ld50)
    return(ld50$cla)
  }
  data_tox_o$cla <-  tox_classification(data_tox_o$DOSE_VALUE)
  table(data_tox_o$cla )
  ld50 <- data_tox_o$cla 
  ld50 <- as.factor(ld50)
  barplot(table(ld50)[c("voilent", "high", "medium", "low")]
       , xlab="acute toxicity classsification"
       , ylab="frequency")
  pie(table(ld50))
  ## check data converted.
  LOAD_FUNC("FitNormalCurve", 1)
  FitNormalCurve(table(ld50),"log(1/(mol/kg))")
  FitNormalCurve(data_tox_o$MOLWEIGHT,"mol weight")
  ## desc get
  str(data_tox_o$Number)
  tail(data_tox_o$Number)
  desc <- Data[data_tox_o$Number,]
  str(desc[,1])
  desc <- desc[,-1]
  dim(desc[, ])
  rm(Data, data_tox)
  ## save new data
  data_tidy = data.frame("ld50_groups"=ld50, desc)
  write.csv(data_tidy
            , "./data_collected_2013.11.21/RTECS_R_O_LD50_7904.desc.csv"
            , quote     = TRUE
  )
  

## Normalize data
  LOAD_FUNC("Normalize_Data",1)
  tmp <- Normalize_Data(desc)
  preProcValues <- as.list(tmp[1])
  acuteDes4     <- as.data.frame(tmp[2])
  
  dim(acuteDes4)
  rm(tmp)
  #desc_selected <- colnames(acuteDes4)
  attributes(acuteDes4)
  # list data to check it
  acuteDes4[1:5, ]

  #### select proper descripers.
  library("randomForest")
  subsets = c(20,30,40, 45:55, 60, 100, 150)
  ## Examples of these functions are included in the package: 
  ##      lmFuncs, rfFuncs, treebagFuncs and nbFuncs.
  ctrl= rfeControl(functions = rfFuncs, method = "cv",
                   verbose = FALSE, returnResamp = "final")
  Profile = rfe(acuteDes4, ld50, sizes = subsets, rfeControl = ctrl)
  print(Profile)
  plot(Profile, type = c("g", "o"))
  Profile$fit
  top(Profile$optVariables)
  acuteDes5<-acuteDes4[,Profile$optVariables]

##simple tree model
#10%fold-model establishment
rpartGrid<-expand.grid(.cp=.05)
fitControl<-trainControl(
  ##10-fold CV
  method="repeatedcv",
  number=10,
  ##repeated three times
  repeats=5,
  ##Save all the resampling results
  returnResamp="all")
DGrpart.10<-train(acuteDes5, ld50,method="rpart",trControl=fitControl,
                  ##Now specify the exact models to evaludata:
                  tuneGrid=rpartGrid)
DGrpart.10

##Foresttree model(multiple-tree model)
#LOO-fold-model establishment
rfGrid<-expand.grid(.interaction.depth=c(1:3),
                    .n.trees=c(10,30,50,70,90,110,130,150),
                    .shrinkage=0.1)
fitControl<-trainControl(
  ##10-fold CV
  method="LOOCV",
  ##repeated three times
  repeats=5,
  ##Save all the resampling results
  returnResamp="all")
DGrfFit.LOO<-train(acuteDes4,ld50,method="rf",
                   trControl=fitControl,
                   ##Now specify the exact models to evaludata:
                   #tuneGrid=rfGrid,
                   ##This last option is actually one
                   ##for gbm() that passes through
                   verbose=FALSE)
DGrfFit.LOO


## group data with method of Hierarchical Clustering 
  LOAD_FUNC("h.clust",ls())
  dist(acuteDes4)
  hclust(dist(acuteDes4),method="ward")
  groups <- h.clust(acuteDes4,"ward",2)
  names(groups)

  groups <- as.matrix(groups)
  head(groups[, 1])
  summary(groups)
  table(groups)


## assemble data.frame for modeling
  acuteDes4 <- as.matrix(acuteDes4)
  str(acuteDes4)
  total_data <- data.frame("class"=groups[,1]
                           ,"acuteCla"=ld50
                           ,"acuteDes4"=I(acuteDes4))
  str(total_data)
  data_1_flag <- total_data$class == 1
  data_1_flag
  data_1      <- data.frame("ld50"=total_data$acuteCla[data_1_flag]
                            ,"des4"=I(total_data$acuteDes4[data_1_flag,]))
  ### make sure data.frame well
  data_1[1:2,]
  total_data[c(1,3),]
  
  data_2_flag <- total_data$class == 2
  data_2      <- data.frame("ld50"=total_data$acuteCla[data_2_flag]
                            ,"des4"=I(total_data$acuteDes4[data_2_flag,]))
  rm(total_data, data_1_flag, data_2_flag)

  data_total <-  data.frame("ld50" = ld50, "des4" = I(acuteDes4))
  str(data_total)


## modeling by step multiple linear regression 
  LOAD_FUNC("StepMLR",1)
  data_lm   <- StepMLR(data_total,"ld50","des4",,)
  str(data_lm)
  # calc the 赤池信息统计量
  AIC(data_lm) 
  # normality test for residuals
  shapiro.test(data_1_lm$residuals) 

  #### Prediction  from linear model 
  Predict_Show <- function (model,testData,val_obs_name) {
    pred_step.acute <- predict.lm(model,newdata=testData)
    x <- as.vector(as.matrix(testData[val_obs_name]))
    y <- pred_step.acute
    print(x)
    print(y)
    print(summary(y))
    plot( x , y
          , xlab="Observed acute value (log(LD50 mg/kg))"
          , ylab="Predicted acute value (log(LD50 mg/kg))",)
    text(x, y, labels=row(as.matrix(x))) # identify points 
    lines(-1:5,-1:5)
    title("Predicted VS Observed LD50")
  }
  
  Predict_Show(data_lm,data_1_test,"ld50")